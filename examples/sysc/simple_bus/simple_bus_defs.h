/*!
 * \file simple_bus_defs.h
 * \brief Definitions needed for simple_bus
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#pragma once

#ifdef SIMPLE_BUS_EXPORTS
#define SIMPLE_BUS_API __declspec(dllexport)
#elif SIMPLE_BUS_USE
#define SIMPLE_BUS_API __declspec(dllimport)
#else
#define SIMPLE_BUS_API
#endif
