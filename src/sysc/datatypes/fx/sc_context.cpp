
#include "sysc/datatypes/fx/sc_context.h"

namespace sc_dt
{

std::vector<sc_global_base *> sc_global_base::m_vec;

sc_global_base::sc_global_base()
{
    m_vec.push_back(this);
}

sc_global_base::~sc_global_base()
{
}

void sc_global_base::clean_up()
{
    std::vector<sc_global_base *>::iterator it;

    for (it = m_vec.begin(); it != m_vec.end(); ++it)
        delete *it;
}

}
