/*!
 * \file sysc/sc_simulation.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2008-2018 Michel Gillet
 * Distributed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * __legal_e__
 * \endcond
 *
 */

#include "sysc/sc_simulation.h"

#define SC_DISABLE_API_VERSION_CHECK // for in-library sc_ver.h inclusion

#include "sysc/kernel/sc_cmnhdr.h"
#include "sysc/kernel/sc_externs.h"
#include "sysc/kernel/sc_except.h"
#include "sysc/kernel/sc_ver.h"
#include "sysc/utils/sc_report.h"
#include "sysc/utils/sc_report_handler.h"
#include "sysc/utils/sc_utils_ids.h"

#include "sysc/kernel/sc_event.h"
#include "sysc/kernel/sc_ver.h"
#include "sysc/utils/sc_mempool.h"
#include "sysc/datatypes/fx/scfx_mant.h"
#include "sysc/datatypes/int/sc_length_param.h"
#include "sysc/datatypes/fx/sc_context.h"
#include "sysc/communication/sc_interface.h"
#include "sysc/kernel/sc_process_handle.h"
#include "sysc/datatypes/int/sc_unsigned.h"
#include "sysc/datatypes/fx/scfx_rep.h"

#include <algorithm>
#include <cstring>
#include <sstream>
#include <vector>

namespace sc_core
{

extern void pln();

static int    argc_copy;	// Copy of argc value passed to sc_elab_and_sim.
static char** argv_copy;	// Copy of argv value passed to sc_elab_and_sim.

static bool sc_in_action = false;

#ifdef USE_SC_SIMULATION
int sc_argc()
{
    return argc_copy;
}

const char* const* sc_argv()
{
    return argv_copy;
}
#endif

std::unique_ptr<sc_simulation::InitHelper> sc_simulation::m_init_helper;
bool sc_simulation::m_init = false;
bool sc_simulation::m_cleaned = false;

sc_simulation::sc_simulation()
{
    if (m_init_helper == nullptr) m_init_helper = std::make_unique<InitHelper>();
}

sc_simulation::sc_simulation(sc_main_with_param_type main_with_param)
    : m_main_with_param(main_with_param)
{
    if (m_init_helper == nullptr) m_init_helper = std::make_unique<InitHelper>();
}

sc_simulation::~sc_simulation()
{
    reset();
}

int sc_simulation::main()
{
    return 1;
}

int sc_simulation::run()
{
    int status = 1;
    

    // Copy argv into a new structure to prevent sc_main from modifying the
    // result returned from sc_argv.
    /*std::vector<char*> argv_call(argc + 1, static_cast<char*>(NULL));
    for (int i = 0; i < argc; ++i)
    {
        std::size_t size = std::strlen(argv[i]) + 1;
        argv_call[i] = new char[size];
        std::copy(argv[i], argv[i] + size, argv_call[i]);
    }*/

    try
    {
        pln();

        // Perform initialization here
        sc_in_action = true;

        if (m_main_with_param != nullptr)
        {
            status = (*m_main_with_param)(m_argc, m_argv);
        }
		else if (m_main_with_param_simul != nullptr)
		{
			status = (*m_main_with_param_simul)(this);
		}
        else
        {
			status = main();
        }

        // Perform cleanup here
        sc_in_action = false;
    }
    catch (const sc_report& x)
    {
        sc_report_handler::get_handler()
        (x, sc_report_handler::get_catch_actions());
    }
    catch (...)
    {
        // translate other escaping exceptions
        std::shared_ptr<sc_report>  err_p = sc_handle_exception();
        if (err_p)
            sc_report_handler::get_handler()
            (*err_p, sc_report_handler::get_catch_actions());
    }

    /*for (int i = 0; i < argc; ++i)
    {
        delete[] argv_call[i];
    } */

    // IF DEPRECATION WARNINGS WERE ISSUED TELL THE USER HOW TO TURN THEM OFF

    if (sc_report_handler::get_count(SC_ID_IEEE_1666_DEPRECATION_) > 0)
    {
        std::stringstream ss;

        const char MSGNL[] = "\n             ";
        const char CODENL[] = "\n  ";

        ss << "You can turn off warnings about" << MSGNL
           << "IEEE 1666 deprecated features by placing this method call" << MSGNL
           << "as the first statement in your sc_main() function:\n" << CODENL
           << "sc_core::sc_report_handler::set_actions( "
           << "\"" << SC_ID_IEEE_1666_DEPRECATION_ << "\"," << CODENL
           << "                                         " /* indent param */
           << "sc_core::SC_DO_NOTHING );"
           << std::endl;

        SC_REPORT_INFO(SC_ID_IEEE_1666_DEPRECATION_, ss.str().c_str());
    }

    return status;
}

void sc_simulation::reset()
{
    if (sc_get_curr_simcontext() != nullptr)
        sc_get_curr_simcontext()->reset();
}

void sc_simulation::set_argv_call(int argc, char* argv[])
{
	argc_copy = argc;
	argv_copy = argv;

	m_argc = argc;
	m_argv = argv;

	m_argv_call.reserve(argc);

	for (int i = 0; i < argc; ++i)
	{
		std::size_t size = std::strlen(argv[i]) + 1;
		m_argv_call.push_back(argv[i]);
	}
}

int sc_simulation::init()
{
    if (m_init == true)
        return -1;

    /*sc_interface::init();
    sc_process_handle::init();
    sc_dt::sc_unsigned::init();
    sc_dt::sc_unsigned_bitref::init();
    sc_dt::sc_unsigned_subref::init();
    sc_dt::scfx_rep::init(); */

    /*sc_report::initialize(); */
    m_init = true;
    m_cleaned = false;
    return 0;
}

int sc_simulation::clean()
{
    if (m_init == false)
        return -1;
    if (m_cleaned == true)
        return -2;

#ifndef PURIFY
    /*if (sc_get_default_simcontext()!=NULL)
    {
        delete sc_get_default_simcontext();
        sc_set_curr_simcontext(NULL);
    }*/
#endif

    sc_core::sc_mempool::clean_up();
    sc_core::sc_event_timed::clean_up();
    sc_dt::scfx_mant::clean_up();
    sc_dt::sc_global_base::clean_up();

    m_cleaned = true;

    return 0;
}

bool *sc_simulation::get_sc_in_action()
{
    return &sc_in_action;
}

bool sc_simulation::is_cleaned()
{
    return m_cleaned;
}

bool sc_simulation::is_init()
{
    return m_init;
}

void sc_simulation::set_display_copyright_message(bool display_copyright_message)
{
    sc_display_copyright_message(display_copyright_message);
}

sc_simulation::InitHelper::InitHelper()
{
    init();
}

sc_simulation::InitHelper::~InitHelper()
{
#ifdef _MSC_VER
    clean();    // Calling clean on Linux seems to create more problems than solving them
#endif
}

}



