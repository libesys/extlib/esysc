import os,sys,shutil,stat
import time

def mycopytree(src, dst, symlinks=False):
    """Recursively copy a directory tree using copy2().

    The destination directory must not already exist.
    If exception(s) occur, an Error is raised with a list of reasons.

    If the optional symlinks flag is true, symbolic links in the
    source tree result in symbolic links in the destination tree; if
    it is false, the contents of the files pointed to by symbolic
    links are copied.

    XXX Consider this example code rather than the ultimate tool.

    """
    #print "src="+src
    #print "dst="+dst
    names = os.listdir(src)
    if not os.path.isdir(dst):
        os.mkdir(dst)    
    errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        if name<>"CVS":
            try:
                if symlinks and os.path.islink(srcname):
                    linkto = os.readlink(srcname)
                    os.symlink(linkto, dstname)
                elif os.path.isdir(srcname):
                    mycopytree(srcname, dstname, symlinks)
                else:
                    src_time=os.stat(srcname)[stat.ST_CTIME]
                    #print "src time = %i %s" % (time,srcname)
                    if os.path.exists(dstname):
                        dst_time=os.stat(dstname)[stat.ST_CTIME]
                    else:
                        dst_time=None
                    #print "dst time = %i %s" % (time,dstname)
                    shutil.copy2(srcname, dstname)
                    shutil.copystat(srcname, dstname)
                # XXX What about devices, sockets etc.?
            except (IOError, os.error), why:
                errors.append((srcname, dstname, why))
    if errors:
        raise shutil.Error, errors

def find_version():
    fin=open("..\\..\\src\\sysc\\version.h","r")
    lines=fin.readlines()
    fin.close()

    for line in lines:
        aaa=line.find("VERSION_NUM_DOT_STRING")
        if aaa<>-1:
            line=line[9+len("VERSION_NUM_DOT_STRING"):]
            aaa=line.find("\"")
            line=line[aaa+1:]
            aaa=line.find("\"")
            line=line[:aaa]
            return line

#devpack="mysystemc201-0.0.4"
devpack="mysystemc232-"+find_version()
devpackdir="..\\..\\build\\vc14_dll\\devkit\\"+devpack

#print current working directory
cwd=os.getcwd()

#print "Current working directory is " + cwd

#delete the devpack dir if it exists

if os.path.isdir(devpackdir):
    shutil.rmtree(devpackdir,False)
if os.path.isdir("..\\..\\include"):
    try:
        shutil.rmtree("..\\..\\include",False)
    except:
        time.sleep(1)       #ugly trick to fix some issues with rmtree
        shutil.rmtree("..\\..\\include",False)
        
#build the directory tree for the DevPak

os.makedirs(devpackdir)
os.chdir(devpackdir)
os.makedirs("bin\\vc14_dll")
os.makedirs("bin\\vc14_x64_dll")
os.mkdir("include")
os.makedirs("lib\\vc14_dll")
os.makedirs("lib\\vc14_x64_dll")
#os.makedirs("include\\sysc\packages")
#os.mkdir("samples")
#os.mkdir("templates")

#copy the package description file 

#shutil.copy(cwd+"\\mysystemc.DevPackage","mysystemc.DevPackage")

#copy the include files to the include directory

include_dir=["sysc",
             "sysc\\communication",
             "sysc\\datatypes\\bit",
             "sysc\\datatypes\\fx",
             "sysc\\datatypes\\int",
             "sysc\\datatypes\\misc",
             "sysc\\kernel",
             "sysc\\packages\\qt",
             "sysc\\packages\\qt\\md",
             "sysc\\tracing",
             "sysc\\utils"]

for aaa in range(len(include_dir)):
    os.makedirs("include\\"+include_dir[aaa])
    os.makedirs(cwd+"\\..\\..\\include\\"+include_dir[aaa])
    files=os.listdir(cwd+"\\..\\..\\src\\"+include_dir[aaa]+"\\")
    for thefile in files:
        if thefile[-2:]==".h":
            shutil.copy2(cwd+"\\..\\..\\src\\"+include_dir[aaa]+"\\"+thefile,"include\\"+include_dir[aaa]+"\\"+thefile)
            shutil.copy2(cwd+"\\..\\..\\src\\"+include_dir[aaa]+"\\"+thefile,cwd+"\\..\\..\\include\\"+include_dir[aaa]+"\\"+thefile)

shutil.copy2(cwd+"\\..\\..\\src\\systemc.h","include\\systemc.h")
shutil.copy2(cwd+"\\..\\..\\src\\systemc","include\\systemc")
#shutil.copy2(cwd+"\\..\\..\\src\\sysc\\systemc_prec.h","include\\sysc\\systemc_prec.h")
#shutil.copy2(cwd+"\\..\\..\\src\\sysc\\systemcclass.h","include\\sysc\\systemcclass.h")
#shutil.copy2(cwd+"\\..\\..\\src\\sysc\\systemc_defs.h","include\\sysc\\systemc_defs.h")

shutil.copy2(cwd+"\\..\\..\\src\\systemc.h",cwd+"\\..\\..\\include\\systemc.h")
shutil.copy2(cwd+"\\..\\..\\src\\systemc",cwd+"\\..\\..\\include\\systemc")
#shutil.copy2(cwd+"\\..\\..\\src\\sysc\\systemc_prec.h",cwd+"\\..\\..\\include\\sysc\\systemc_prec.h")
#shutil.copy2(cwd+"\\..\\..\\src\\sysc\\systemcclass.h",cwd+"\\..\\..\\include\\sysc\\systemcclass.h")
#shutil.copy2(cwd+"\\..\\..\\src\\sysc\\systemc_defs.h",cwd+"\\..\\..\\include\\sysc\\systemc_defs.h")

#copy the boost library used internally

mycopytree(cwd+"\\..\\..\\src\\sysc\\packages",cwd+"\\..\\..\\include\\sysc\\packages")
mycopytree(cwd+"\\..\\..\\src\\sysc\\packages","include\\sysc\\packages")

#copy dlls in the bin directory

dlls=["mysystemc232.dll","mysystemc232d.dll"]

for aaa in range(len(dlls)):
    shutil.copy(cwd+"\\..\\..\\lib\\vc14_dll\\"+dlls[aaa],"bin\\vc14_dll\\"+dlls[aaa])
    shutil.copy(cwd+"\\..\\..\\lib\\vc14_x64_dll\\"+dlls[aaa],"bin\\vc14_x64_dll\\"+dlls[aaa])

#copy libs in the lib directory

libs=["mysystemc232.lib","mysystemc232d.lib", "mysystemc232d.pdb"]

for aaa in range(len(libs)):
    shutil.copy(cwd+"\\..\\..\\lib\\vc14_dll\\"+libs[aaa],"lib\\vc14_dll\\"+libs[aaa])
    shutil.copy(cwd+"\\..\\..\\lib\\vc14_x64_dll\\"+libs[aaa],"lib\\vc14_x64_dll\\"+libs[aaa])

#copy the samples to the samples directory

#mycopytree(cwd+"\\..\\samples","samples")

#copy the samples to the templates directory


#mycopytree(cwd+"\\..\\templates","templates")

#mycopytree("include",cwd+"\\..\\..\\include")

os.chdir(cwd)

#end







